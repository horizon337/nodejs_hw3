const express = require('express');

const trucks = require('../../controllers/trucks');

const { ctrlWrapper } = require('../../helpers');

const { validation, isValidId, driver } = require('../../middlewares');

const { addTruck } = require('../../schemas');

const router = express.Router();

router.get('/', driver, ctrlWrapper(trucks.truckList));

router.get('/:id', driver, isValidId, ctrlWrapper(trucks.getById));

router.post('/', driver, validation(addTruck), ctrlWrapper(trucks.addTruck));

router.put('/:id', driver, isValidId, ctrlWrapper(trucks.updateTruck));

router.delete('/:id', driver, isValidId, ctrlWrapper(trucks.deleteTruck));

router.post('/:id/assign', driver, isValidId, ctrlWrapper(trucks.assignTruck));

module.exports = router;
