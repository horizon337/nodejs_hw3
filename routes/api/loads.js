const express = require('express');

const loads = require('../../controllers/loads');

const { ctrlWrapper } = require('../../helpers');

const {
  validation, isValidId, driver, shipper, user,
} = require('../../middlewares');

const { addLoad } = require('../../schemas');

const router = express.Router();

router.get('/', user, ctrlWrapper(loads.loadList));

router.post('/', shipper, validation(addLoad), ctrlWrapper(loads.addLoad));

router.get('/active', driver, ctrlWrapper(loads.getActive));

router.patch('/active/state', driver, ctrlWrapper(loads.changeState));

router.get('/:id', user, isValidId, ctrlWrapper(loads.getById));

router.put('/:id', shipper, isValidId, ctrlWrapper(loads.updateLoad));

router.delete('/:id', shipper, isValidId, ctrlWrapper(loads.deleteLoad));

router.post('/:id/post', shipper, isValidId, ctrlWrapper(loads.addLoadById));

router.get('/:id/shipper_info', shipper, isValidId, ctrlWrapper(loads.getShipperInfo));

module.exports = router;
