const Joi = require('joi');

const addTruck = Joi.object({
  type: Joi.string().required(),
});

module.exports = addTruck;
