const registerSchema = require('./registerSchema');
const loginSchema = require('./loginSchema');
const patchSchema = require('./patchSchema');
const emailSchema = require('./emailSchema');

module.exports = {
  registerSchema,
  loginSchema,
  patchSchema,
  emailSchema,
};
