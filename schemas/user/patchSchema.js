const Joi = require('joi');

const patchSchema = Joi.object({
  oldPassword: Joi.string().min(4),
  newPassword: Joi.string().min(4),
});

module.exports = patchSchema;
