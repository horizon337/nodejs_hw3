const { addTruck, updateStatusNote } = require('./truck');
const {
  registerSchema, loginSchema, patchSchema, emailSchema,
} = require('./user');
const { addLoad } = require('./loads');

module.exports = {
  addTruck,
  updateStatusNote,
  registerSchema,
  loginSchema,
  patchSchema,
  emailSchema,
  addLoad,
};
