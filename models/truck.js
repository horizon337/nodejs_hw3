const { Schema, model } = require('mongoose');

const truckSchema = Schema({
  created_by: {
    type: Schema.Types.ObjectId,
    ref: 'user',
    required: true,
  },
  assigned_to: {
    type: Schema.Types.ObjectId,
    ref: 'user',
    default: null,
  },
  type: {
    type: String,
    enum: ['SPRINTER', 'SMALL STRAIGHT', 'LARGE STRAIGHT'],
    require: true,
  },
  status: {
    type: String,
    enum: ['OL', 'IS'],
    required: true,
    default: 'IS',
  },
  dimensions: {
    width: {
      type: Number,
    },
    length: {
      type: Number,
    },
    height: {
      type: Number,
    },
  },
  payload: {
    type: Number,
  },
}, { versionKey: false, timestamps: { createdAt: 'created_date', updatedAt: false } });

const Truck = model('truck', truckSchema);

module.exports = {
  Truck,
};
