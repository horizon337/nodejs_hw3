const { Truck } = require('./truck');
const { User } = require('./user');

module.exports = {
  Truck,
  User,
};
