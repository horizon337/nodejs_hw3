const { Schema, model } = require('mongoose');

const loadSchema = Schema({
  created_by: {
    type: Schema.Types.ObjectId,
    ref: 'user',
    require: true,
  },
  assigned_to: {
    type: String,
  },
  status: {
    type: String,
    enum: ['NEW', 'POSTED', 'ASSIGNED', 'SHIPPED'],
    required: true,
    default: 'NEW',
  },
  state: {
    type: String,
    enum: ['En route to Pick Up', 'Arrived to Pick Up', 'En route to delivery', 'Arrived to delivery'],
  },
  name: {
    type: String,
    required: true,
  },
  payload: {
    type: Number,
    required: true,
  },
  pickup_address: {
    type: String,
    required: true,
  },
  delivery_address: {
    type: String,
    required: true,
  },
  dimensions: {
    width: {
      type: Number,
      required: true,
    },
    length: {
      type: Number,
      required: true,
    },
    height: {
      type: Number,
      required: true,
    },
  },
  logs: [{
    message: {
      type: String,
    },
    time: {
      type: Date,
    },
  }],
}, { versionKey: false, timestamps: { createdAt: 'created_date', updatedAt: false } });

const Load = model('load', loadSchema);

module.exports = {
  Load,
};
