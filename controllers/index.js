const trucks = require('./trucks');
const users = require('./users');
const auth = require('./auth');
const loads = require('./loads');

module.exports = {
  trucks,
  users,
  auth,
  loads,
};
