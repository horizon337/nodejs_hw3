const { createError } = require('../../helpers');

const { Load } = require('../../models/load');

const getById = async (req, res) => {
  const { id } = req.params;
  const load = await Load.findById(id);
  if (!load) {
    throw createError(404);
  }
  res.json({
    load,
  });
};

module.exports = getById;
