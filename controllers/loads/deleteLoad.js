const { createError } = require('../../helpers');

const { Load } = require('../../models/load');

const removeNote = async (req, res) => {
  const { id } = req.params;
  const result = await Load.findByIdAndRemove(id);
  if (!result) {
    throw createError(404);
  }
  res.status(200).json({
    message: 'Load deleted successfully',
  });
};

module.exports = removeNote;
