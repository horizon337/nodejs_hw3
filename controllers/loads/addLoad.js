const { Load } = require('../../models/load');

const addLoad = async (req, res) => {
  const { _id } = req.user;
  const {
    name, payload, pickup_address, delivery_address, dimensions,
  } = req.body;
  await Load.create({
    name,
    payload,
    pickup_address,
    delivery_address,
    dimensions,
    created_by: _id,
    logs: {
      message: 'Load status \'NEW\'',
      time: new Date().toISOString(),
    },
  });
  res.status(200).json({
    message: 'Load created successfully',
  });
};

module.exports = addLoad;
