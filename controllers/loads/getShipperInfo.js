const { createError } = require('../../helpers');

const { Load } = require('../../models/load');
const { Truck } = require('../../models/truck');

const getById = async (req, res) => {
  const { id } = req.params;
  const load = await Load.findById(id);
  if (!load) {
    throw createError(400);
  }
  const { assigned_to } = load;
  const truck = await Truck.findById(assigned_to);
  res.json({
    load,
    truck,
  });
};

module.exports = getById;
