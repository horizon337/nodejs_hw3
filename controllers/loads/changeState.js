const { createError } = require('../../helpers');
const { Load } = require('../../models/load');
const { Truck } = require('../../models/truck');

const changeState = async (req, res) => {
  const { _id } = req.user;

  const stateArray = ['En route to Pick Up', 'Arrived to Pick Up', 'En route to delivery', 'Arrived to delivery'];
  const activeLoad = await Load.findOne({ assigned_to: _id, status: 'ASSIGNED' });

  if (!activeLoad) {
    throw createError(400);
  }

  const { state } = activeLoad;
  const index = stateArray.indexOf(state);
  await activeLoad.updateOne({
    state: stateArray[index + 1],
    $push: {
      logs: {
        message: `Load state changed to ${stateArray[index + 1]}`,
        time: new Date().toISOString(),
      },
    },
  });
  if (stateArray.indexOf(state) === 2) {
    await activeLoad.updateOne({
      status: 'SHIPPED',
      $push: {
        logs: {
          message: 'Load status changed to \'SHIPPED\'',
          time: new Date().toISOString(),
        },
      },
    });
    const { assigned_to } = activeLoad;
    await Truck.findOneAndUpdate({ _id: assigned_to }, {
      status: 'IS',
    });
  }
  res.status(200).json({
    message: 'Load details changed successfully',
  });
};

module.exports = changeState;
