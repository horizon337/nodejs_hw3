const addLoad = require('./addLoad');
const loadList = require('./loadList');
const deleteLoad = require('./deleteLoad');
const updateLoad = require('./updateLoad');
const getById = require('./getById');
const addLoadById = require('./addLoadById');
const changeState = require('./changeState');
const getActive = require('./getActive');
const getShipperInfo = require('./getShipperInfo');

module.exports = {
  addLoad,
  loadList,
  deleteLoad,
  updateLoad,
  getById,
  addLoadById,
  getActive,
  changeState,
  getShipperInfo,
};
