const { Load } = require('../../models/load');

const loadList = async (req, res) => {
  const { _id } = req.user;
  const { offset = 0, limit = 10, status } = req.query;
  const query = { $or: [{ created_by: _id }, { assigned_to: _id }] };
  let loads;
  if (status) {
    loads = await Load.find({ query, status }, '', { skip: Number(offset), limit: Number(limit) });
  }
  loads = await Load.find(query, '', { skip: Number(offset), limit: Number(limit) });

  res.json({
    loads,
  });
};

module.exports = loadList;
