const { createError } = require('../../helpers');

const { Load } = require('../../models/load');
const { Truck } = require('../../models/truck');

const addLoadById = async (req, res) => {
  const { id } = req.params;
  const load = await Load.findById(id);
  if (!load || load.status === 'ASSIGNED') {
    throw createError(400);
  }
  await load.updateOne({
    status: 'POSTED',
    $push: {
      logs: {
        message: 'Load status changed to \'POSTED\'',
        time: new Date().toISOString(),
      },
    },
  });
  const {
    width, length, height,
  } = load.dimensions;
  const { payload } = load;

  const truck = await Truck.findOne({
    status: {
      $eq: 'IS',
    },
    assigned_to: { $ne: null },
    'dimensions.width': {
      $gte: width,
    },
    'dimensions.length': {
      $gte: length,
    },
    'dimensions.height': {
      $gte: height,
    },
    payload: {
      $gte: payload,
    },
  });
  if (!truck) {
    await load.updateOne({
      status: 'NEW',
      $push: {
        logs: {
          message: 'Load status changed to \'NEW\'',
          time: new Date().toISOString(),
        },
      },
    });
    throw createError(400, 'There are no free trucks');
  }

  const { created_by } = truck;
  await load.updateOne({
    assigned_to: created_by,
    status: 'ASSIGNED',
    state: 'En route to Pick Up',
    $push: {
      logs: {
        message: `Load assigned to driver with id ${created_by}`,
        time: new Date(),
      },
    },
  });

  await truck.updateOne({
    status: 'OL',
  });

  res.json({
    message: 'Load posted successfully',
    driver_found: true,
  });
};

module.exports = addLoadById;
