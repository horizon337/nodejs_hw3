const bcrypt = require('bcryptjs');
const { User } = require('../../models');
const createError = require('../../helpers/createError');
const { registerSchema } = require('../../schemas');

const register = async (req, res) => {
  const { error } = registerSchema.validate(req.body);
  if (error) {
    throw createError(400, error.message);
  }
  const { email, password, role } = req.body;
  const user = await User.findOne({ email });
  if (user) {
    throw createError(400, 'Email in use');
  }
  const hashPassword = bcrypt.hashSync(password, bcrypt.genSaltSync(10));
  await User.create({ email, password: hashPassword, role });
  res.status(200).json({
    message: 'Profile created successfully',
  });
};

module.exports = register;
