const bcrypt = require('bcryptjs');
const randomize = require('randomatic');
const { createError, sendEmail } = require('../../helpers');

const { User } = require('../../models');

const { emailSchema } = require('../../schemas');

const sendPassword = async (req, res) => {
  const { error } = emailSchema.validate(req.body);
  if (error) {
    throw createError(400, error.message);
  }
  const { email } = req.body;
  const user = await User.findOne({ email });
  if (!user) {
    throw createError(400, 'Email is wrong');
  }
  const newPassword = randomize('Aa0)', 8);
  const hashPassword = bcrypt.hashSync(newPassword, bcrypt.genSaltSync(10));
  await User.findOneAndUpdate(email, { password: hashPassword });

  const mail = {
    to: email,
    subject: 'New password',
    html: `<p>Your new password: <b>${newPassword}</b></p>`,
  };

  await sendEmail(mail);

  res.status(200).json({
    message: 'New password sent to your email address',
  });
};

module.exports = sendPassword;
