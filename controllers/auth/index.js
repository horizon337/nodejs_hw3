const login = require('./login');
const register = require('./register');
const sendPassword = require('./sendPassword');

module.exports = {
  login,
  register,
  sendPassword,
};
