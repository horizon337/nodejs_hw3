const { createError } = require('../../helpers');

const { Truck } = require('../../models/truck');

const assignTruck = async (req, res) => {
  const { _id } = req.user;
  const { id } = req.params;
  const truck = await Truck.findById(id);
  if (!truck) {
    throw createError(400);
  }

  const otherTruck = await Truck.findOne({ id, assigned_to: _id });
  if (otherTruck) {
    throw createError(400);
  }

  await truck.updateOne({ assigned_to: _id });

  res.json({
    message: 'Truck assigned successfully',
  });
};

module.exports = assignTruck;
