const { createError } = require('../../helpers');

const { Truck } = require('../../models/truck');

const updateTruck = async (req, res) => {
  const { id } = req.params;
  const type = req.body;
  const result = await Truck.findByIdAndUpdate(id, type, { new: true });
  if (!result || result.assigned_to) {
    throw createError(400);
  }
  res.status(200).json({
    message: 'Truck details changed successfully',
  });
};

module.exports = updateTruck;
