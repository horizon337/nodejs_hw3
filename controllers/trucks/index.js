const addTruck = require('./addTruck');
const delereTruck = require('./deleteTruck');
const getById = require('./getById');
const truckList = require('./truckList');
const updateTruck = require('./updateTruck');
const assignTruck = require('./assignTruck');

module.exports = {
  addTruck,
  delereTruck,
  getById,
  truckList,
  updateTruck,
  assignTruck,
};
