const { Truck } = require('../../models/truck');

const truckList = async (req, res) => {
  const { _id } = req.user;
  const query = { created_by: _id };
  const trucks = await Truck.find(query);

  res.json({
    trucks,
  });
};

module.exports = truckList;
