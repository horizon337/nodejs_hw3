const { Truck } = require('../../models/truck');

const addTruck = async (req, res) => {
  const { _id } = req.user;
  const { type } = req.body;
  let dimensions;
  let payload;
  if (type === 'SPRINTER') {
    dimensions = {
      width: 250,
      length: 250,
      height: 170,
    };
    payload = 1700;
  }
  if (type === 'SMALL SPRAIGHT') {
    dimensions = {
      width: 500,
      length: 250,
      height: 170,
    };
    payload = 2500;
  }
  if (type === 'LARGE SPRAIGHT') {
    dimensions = {
      width: 700,
      length: 350,
      height: 200,
    };
    payload = 4000;
  }
  await Truck.create({
    type, dimensions, payload, created_by: _id,
  });
  res.status(200).json({
    message: 'Truck created successfully',
  });
};

module.exports = addTruck;
